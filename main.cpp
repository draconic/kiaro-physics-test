/*
    main.cpp

    Test suite code for the physics sub system.

    This software is licensed under the GNU Lesser General
    Public License version 3. Please refer to gpl.txt and
    lgpl.txt for more information.

    Copyright (c) 2013 Draconic Entertainment
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

extern "C"
{
#include <KiaroPhysics.h>
}

// To provide a visual representation
#include <irrlicht.h>

void on_update(const KiaroPhysicsVector3F position, const KiaroPhysicsVector3F rotation)
{
    printf("%f\n", position.f32_y);
}

int main(int argc, char *argv[])
{
    unsigned int result = 0;

    // Set up the renderer
    irr::IrrlichtDevice *device = irr::createDevice(irr::video::EDT_OPENGL, irr::core::dimension2d<unsigned int>(640,480),
                                                    16, false, false, false);
    if (!device)
    {
        printf("FATAL: Failed to initialize Irrlicht rendering context!\n");
        return 1;
    }

    irr::video::IVideoDriver *video = device->getVideoDriver();
    irr::scene::ISceneManager *smgr = device->getSceneManager();

    // Configure the scene
    irr::scene::ICameraSceneNode *camera = smgr->addCameraSceneNodeFPS();
    smgr->setActiveCamera(camera);

    irr::scene::IMeshSceneNode *cube = smgr->addCubeSceneNode(5);
    cube->setMaterialFlag(irr::video::EMF_LIGHTING, false);

    irr::scene::IMeshSceneNode *ground = smgr->addCubeSceneNode(10);
    ground->setPosition(irr::core::vector3df(0,-100,0));
    ground->setMaterialFlag(irr::video::EMF_LIGHTING, false);

    // Configure Bullet
    assert(KiaroPhysicsInitialize() == KIARO_PHYSICS_ERROR_NONE);

    KiaroPhysicsRigidBody *moving_cube = (KiaroPhysicsRigidBody*)malloc(sizeof(KiaroPhysicsRigidBody));
    assert(KiaroPhysicsRigidBodyCreateCube(KiaroPhysicsVector3FCreate(5,5,5), 10, moving_cube) == KIARO_PHYSICS_ERROR_NONE);
    moving_cube->on_update = on_update;


    float last = 0;
    while (device->run())
    {
        float current = clock();
        assert(KiaroPhysicsUpdate((current-last)/CLOCKS_PER_SEC) == KIARO_PHYSICS_ERROR_NONE);
        last = current;

        video->beginScene();
        smgr->drawAll();
        video->endScene();
    }


    KiaroPhysicsDeinitialize();
    device->drop();
    return 0;
}
